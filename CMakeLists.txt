cmake_minimum_required(VERSION 3.5.1)

add_compile_options(-Wall -Wextra -Wno-unused-parameter -Wfatal-errors -Werror)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++17")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

project(main)

add_subdirectory(components)
add_subdirectory(debug)
