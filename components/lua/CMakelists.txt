file(GLOB_RECURSE SOURCES_LUA LIST_DIRECTORIES true
	lua/lapi.c
	lua/lauxlib.c
	lua/lbaselib.c
	lua/lcode.c
	lua/lcorolib.c
	lua/lctype.c
	lua/ldblib.c
	lua/ldebug.c
	lua/ldo.c
	lua/ldump.c
	lua/lfunc.c
	lua/lgc.c
	lua/linit.c
	lua/liolib.c
	lua/llex.c
	lua/lmathlib.c
	lua/lmem.c
	lua/loadlib.c
	lua/lobject.c
	lua/lopcodes.c
	lua/loslib.c
	lua/lparser.c
	lua/lstate.c
	lua/lstring.c
	lua/lstrlib.c
	lua/ltable.c
	lua/ltablib.c
	lua/ltm.c
	lua/lundump.c
	lua/lutf8lib.c
	lua/lvm.c
	lua/lzio.c
)

add_definitions(-DLUA_USE_LINUX -fPIE)

add_library(lua_core STATIC ${SOURCES_LUA} lua.cpp)

target_link_libraries(lua_core PUBLIC -ldl)

target_include_directories(lua_core PUBLIC
  .
)
