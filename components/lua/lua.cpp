#include "lua.h"
extern "C" {
#include "lua/lstate.h"
#include "lua/lua.h"
#include "lua/lualib.h"
#include "lua/lauxlib.h"
}
#include <assert.h>

/** @ref https://zhuanlan.zhihu.com/p/98134347 */
lua::lua() :
_lua((void *)luaL_newstate()) {
  ((void **)_lua)[-1] = this;
  luaL_openlibs((lua_State *)_lua);
}

lua::~lua() {
  lua_close((lua_State *)_lua);
}

int lua::dostring(const char *str) {
  return luaL_dostring((lua_State *)_lua, str);
}

int lua::dofile(const char *filename) {
  return luaL_dofile((lua_State *)_lua, filename);
}

int lua::func_wrap(void *l) {
  return get(l)->_func(get(l));
}

lua *lua::get(void *l) {
	return (lua *)((void **)l)[-1];
}

void lua::newlib(const reg_t *funcs) {
  lua_newtable((lua_State *)_lua);
  luaL_setfuncs((lua_State *)_lua, (const luaL_Reg *)funcs, 0);
}

void lua::requiref(const char name[], int(*func)(lua *l), bool global) {
  _func = func;
  luaL_requiref((lua_State *)_lua, name, (lua_CFunction)func_wrap, global);
}

int lua::getglobal(const char name[]) {
  return lua_getglobal((lua_State *)_lua, name);
}

int lua::gettop() {
  return lua_gettop((lua_State *)_lua);
}

void lua::call(int nargs, int nresults) {
  lua_call((lua_State *)_lua, nargs, nresults);
}

void lua::pushboolean(bool val) {
  lua_pushboolean((lua_State *)_lua, val);
}

void lua::pushinteger(int64_t val) {
  lua_pushinteger((lua_State *)_lua, (lua_Integer)val);
}

void lua::pushnumber(double val) {
  lua_pushnumber((lua_State *)_lua, val);
}

void lua::pushstring(const char *str) {
  lua_pushstring((lua_State *)_lua, str);
}

bool lua::toboolean(int idx) {
  return lua_toboolean((lua_State *)_lua, idx);
}

int64_t lua::tointeger(int idx) {
  return lua_tointeger((lua_State *)_lua, idx);
}

double lua::tonumber(int idx) {
  return lua_tonumber((lua_State *)_lua, idx);
}

std::string lua::tostring(int idx) {
  return lua_tostring((lua_State *)_lua, idx);
}

void lua::pop(int idx) {
  lua_pop((lua_State *)_lua, idx);
}

void lua::stack_dump() {
  printf(">>> begin dump lua stack\n");
  int i = 0;
  int top = lua_gettop((lua_State *)_lua);
  for (i = 1; i <= top; ++i) {
    int t = lua_type((lua_State *)_lua, i);
    switch (t) {
      case LUA_TSTRING:
        printf("'%s'\n", lua_tostring((lua_State *)_lua, i));
        break;
      case LUA_TBOOLEAN:
        printf(lua_toboolean((lua_State *)_lua, i) ? "true\n" : "false\n");
        break;
      case LUA_TNUMBER:
        printf("%g\n", lua_tonumber((lua_State *)_lua, i));
        break;
      default:
        printf("%s\n", lua_typename((lua_State *)_lua, t));
        break;
    }
  }
  printf(">>> end dump lua stack\n");
}
