#pragma once

#include <type_traits>
#include <tuple>
#include <string>
#include <map>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

class lua {
public:
  template<typename T,typename... Types>
  struct is_tuple {
    static const bool value = false;
  };
  template<typename T,typename... Types>
  struct is_tuple<std::tuple<T, Types...>> {
    static const bool value = true;
  };
  struct reg_t {
    const char *name;
    int(*func)(void *l);
  };
  lua();
  ~lua();
  int dostring(const char *str);
  int dofile(const char *filename);
  void newlib(const reg_t *funcs);
  void requiref(const char name[], int(*func)(lua *l), bool global = true);
  static lua *get(void *l);

  template <typename res_t, typename... arg_t>
  int call(res_t(*func)(arg_t ...));
  template <typename ... res_t, typename ... arg_t>
  std::tuple<res_t ...> call(const char func[], arg_t ... args);
 private:
  void *_lua = nullptr;
  int(*_func)(lua *l);
  static int func_wrap(void *l);
  void stack_dump();
  int getglobal(const char name[]);
  int gettop();
  void call(int nargs, int nresults);

  void pushboolean(bool val);
  void pushinteger(int64_t val);
  void pushnumber(double val);
  void pushstring(const char *str);

  bool toboolean(int idx);
  int64_t tointeger(int idx);
  double tonumber(int idx);
  std::string tostring(int idx);
  void pop(int idx);

  template <typename res_t>
  res_t to(int idx);
  template <typename res_t>
  res_t torev(int &idx) {
    res_t res = to<res_t>(idx);
    idx--;
    return res;
  }
  template <typename arg_t>
  void push(arg_t arg);
  template <typename ... arg_t>
  void push(arg_t ... args);
};

template <typename res_t, typename ... arg_t>
int lua::call(res_t(*func)(arg_t ...)) {
  /* void() */
  if constexpr (std::is_void<res_t>::value && sizeof...(arg_t) == 0) {
    func();
    return 0;
  }
  else {
    int idx = gettop();
    auto args = std::make_tuple(torev<arg_t>(idx) ...);
    /* void(...) */
    if constexpr (std::is_void<res_t>::value) {
      std::apply(func, args);
      return 0;
    }
    /* std::tuple<...>(...) */
    else if constexpr (is_tuple<res_t>::value) {
      res_t res = std::apply(func, args);
      push(std::get<arg_t>(res) ...);
      return std::tuple_size<res_t>::value;
    }
    /* type(...) */
    else {
      res_t res = std::apply(func, args);
      push(res);
      return 1;
    }
  }
}

template <typename ... res_t, typename ... arg_t>
std::tuple<res_t ...> lua::call(const char func[], arg_t ... args) {
  assert(getglobal(func));
  push(args ...);
  call(sizeof...(arg_t), sizeof...(res_t));
  return std::make_tuple(to<res_t>(-1) ...);
}

template <typename arg_t>
void lua::push(arg_t arg) {
  if constexpr (std::is_same<arg_t, bool>::value)
    pushboolean(arg);
  else if constexpr (
         std::is_same<arg_t, int8_t>::value
      || std::is_same<arg_t, int16_t>::value
      || std::is_same<arg_t, int32_t>::value
      || std::is_same<arg_t, int64_t>::value
      || std::is_same<arg_t, uint8_t>::value
      || std::is_same<arg_t, uint16_t>::value
      || std::is_same<arg_t, uint32_t>::value
      || std::is_same<arg_t, uint64_t>::value
    )
    pushinteger(arg);
  else if constexpr (
       std::is_same<arg_t, float>::value
    || std::is_same<arg_t, double>::value
  )
    pushnumber(arg);
  else if constexpr (std::is_same<arg_t, const char *>::value)
    pushstring(arg);
  else if constexpr (std::is_same<arg_t, std::string>::value)
	pushstring(arg.c_str());
  else
	assert(false);
}

template <typename ... arg_t>
void lua::push(arg_t ... args) {
  (push(args), ...);
}

template <typename res_t>
res_t lua::to(int idx) {
  res_t res;
  if constexpr (std::is_same<res_t, bool>::value)
    // printf("idx %d ", idx), res = toboolean(idx), printf("bool %d\n", res);
	  res = toboolean(idx);
  else if constexpr (
       std::is_same<res_t, int8_t>::value
    || std::is_same<res_t, int16_t>::value
    || std::is_same<res_t, int32_t>::value
    || std::is_same<res_t, int64_t>::value
    || std::is_same<res_t, uint8_t>::value
    || std::is_same<res_t, uint16_t>::value
    || std::is_same<res_t, uint32_t>::value
    || std::is_same<res_t, uint64_t>::value
  )
    // printf("idx %d ", idx), res = (res_t)tointeger(idx), printf("integer %d\n", res);
	  res = (res_t)tointeger(idx);
  else if constexpr (
       std::is_same<res_t, float>::value
    || std::is_same<res_t, double>::value
  )
    // printf("idx %d ", idx), res = (res_t)tonumber(idx), printf("number %f\n", res);
	  res = (res_t)tonumber(idx);
  else if constexpr (std::is_same<res_t, std::string>::value)
    // printf("idx %d ", idx), res = tostring(idx), printf("string %s\n", res.c_str());
	  res = tostring(idx);
  else
    assert(false);
  pop(1);
  return res;
}
