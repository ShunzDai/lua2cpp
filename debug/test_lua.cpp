#include "gtest/gtest.h"
#include "lua.h"
// #include "mid_os.h"
#include <stdio.h>

std::tuple<std::string, int, float> func(std::string a, int b, float c) {
  EXPECT_STREQ(a.c_str(), "hello from lua");
  EXPECT_EQ(b, 123);
  EXPECT_FLOAT_EQ(c, 3.14f);
  return {"hello from cpp", b * 2, c * 2};
}

void voidfunc() {
  printf("into %s\n", __FUNCTION__);
}

int wrapper_func(void *l) {
  return lua::get(l)->call(func);
}

int wrapper_voidfunc(void *l) {
  return lua::get(l)->call(voidfunc);
}

int lib_test(lua *l) {
  printf("into %s\n", __func__);
  lua::reg_t reg[] = {
    {"func", wrapper_func},
    {"voidfunc", wrapper_voidfunc},
    {NULL, NULL}
  };
  l->newlib(reg);
  return 1;
}

TEST(lua, call_lua) {
  lua l;
  EXPECT_EQ(l.dostring("\
    function testfunc(a, b, c)\n\
      return \"hello from lua\", b * 2, c * 2\n\
    end\n\
  "), 0);
  auto [a, b, c] = l.call<std::string, int, float>("testfunc", "hello from cpp", 123, 3.14f);
  EXPECT_STREQ(a.c_str(), "hello from lua");
  EXPECT_EQ(b, 246);
  EXPECT_FLOAT_EQ(c, 6.28f);
}

TEST(lua, call_cpp) {
  lua l;
  l.requiref("test", lib_test);
  printf("123412341234\n");
  EXPECT_EQ(l.dostring("\
    a, b, c = test.func(\"hello from lua\", 123, 3.14)\n\
    print(a, b, c)\n\
    test.voidfunc()\n\
  "), 0);
}
