#include "gtest/gtest.h"
#include <stdlib.h>
#include <mcheck.h>

int main(int argc, char *argv[]) {
  setenv("MALLOC_TRACE", "output", 1);
  mtrace();
  
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
